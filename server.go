package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"os"
)

type LoginInfo struct {
	Email string
	Id    string
}

func main() {
	os.Mkdir("./feeds", os.ModeDir|os.ModePerm)

	r := mux.NewRouter()
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "index.html")
	})
	r.HandleFunc("/style.css", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "style.css")
	})
	r.HandleFunc("/{feed}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		feed := Feed(vars["feed"])
		t, err := template.ParseFiles("feed.html")
		if err != nil {
			log.Println(err)
			w.WriteHeader(500)
			return
		}
		err = t.Execute(w, map[string]interface{}{
			"Feed": feed,
		})
		if err != nil {
			log.Println(err)
			return
		}
	})
	r.HandleFunc("/{feed}/", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		w.Header().Set("Location", "/"+vars["feed"])
		w.WriteHeader(300)
	})
	r.HandleFunc("/{feed}/{article}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		feed := Feed(vars["feed"])
		articleId := vars["article"]
		t, err := template.ParseFiles("article.html")
		if err != nil {
			log.Println(err)
			w.WriteHeader(500)
			return
		}

		article, err := feed.Get(articleId)
		if err != nil {
			log.Println(err)
			w.WriteHeader(404)
			return
		}

		err = t.Execute(w, map[string]interface{}{
			"Feed":      feed,
			"ArticleID": articleId,
			"Article":   article,
		})
		if err != nil {
			log.Println(err)
			return
		}
	})

	http.Handle("/", r)
	fmt.Println("http://localhost:5555/")
	err := http.ListenAndServe(":5555", nil)
	os.Stderr.WriteString(err.Error())
}

package main

import (
	"encoding/json"
	"html/template"
	"io/ioutil"
	"strings"
	//  "github.com/microcosm-cc/bluemonday"
	//  "github.com/gomarkdown/markdown"
)

type Feed string

func (f Feed) Data() (*FeedData, error) {
	body, err := ioutil.ReadFile("./feeds/" + string(f) + "/data.json")
	if err != nil {
		return nil, err
	}

	out := new(FeedData)
	err = json.Unmarshal(body, &out)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (f Feed) ID() string {
	return string(f)
}

func (f Feed) Get(item string) (*Article, error) {
	body, err := ioutil.ReadFile("./feeds/" + string(f) + "/article-" + item + ".md")
	if err != nil {
		return nil, err
	}

	out := new(Article)
	if len(body) <= 2 {
		return out, nil
	}
	if body[0] == '#' {
		i := strings.Index(string(body), "\n")
		if i < 0 {
			out.Title = string(body[2:])
		} else {
			out.Title = string(body[2:i])
			out.Content = string(body[i+1:])
		}
	} else {
		out.Content = string(body)
	}

	return out, nil
}

func (f Feed) New() string {
	return ""
}

func (f Feed) Delete(item string) error {
	return nil
}

func (f Feed) Edit(item string, newval string) error {
	return nil
}

type FeedData struct {
	Name        string   `json:"name"`
	Email       string   `json:"email"`
	Color       string   `json:"color"`
	Font        string   `json:"font"`
	Description string   `json:"description"`
	Items       []string `json:"items"`
}

type Article struct {
	Title   string
	Content string
}

func (a *Article) Type() string {
	if len(a.Title) == 0 {
		return "post"
	}
	return "article"
}

func (a *Article) Summary(link string) template.HTML {
	if a.Type() == "article" {
		out := "\n<p>" + a.Content + "</p>" // TODO: Trim to first paragraph
		if link == "" {
			return template.HTML("<h1>" + a.Title + "</h1>" + out)
		} else {
			return template.HTML("<h1><a href=\"" + link + "\">" + a.Title + "</a></h1>" + out)
		}
	} else {
		return template.HTML("<p>" + a.Content + "</p>")
	}
}

func (a *Article) HTML() template.HTML {
	return template.HTML("<h1>" + a.Title + "</h1>\n<p>" + a.Content + "</p>")
}
